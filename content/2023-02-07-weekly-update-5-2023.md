+++
title = "Weekly GNU-like Mobile Linux Update (5/2023): FOSDEM 2023, Sailfish OS 4.5.0 and Sxmo 1.13"
draft = false
date = "2023-02-06T17:35:00Z"
[taxonomies]
tags = ["FOSDEM 2023", "Sailfish OS", "Sxmo", "Phosh", "GNOME Shell on Mobile", "Plasma Mobile",  ]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: A new instance of Ubuntu Touch's newsletter, Phosh 0.24.0, progress with merging GNOME Shell on Mobile into upstream GNOME, and libcamera-powered photography on the PinePhone Pro!
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#81 FOSDEM Weekend](https://thisweek.gnome.org/posts/2023/02/twig-81/)
- [Guido Günther: Phosh 0.24.0 released](https://honk.sigxcpu.org/con/Phosh_0_24_0_released.html)
- [Gnome Shell mobile fork](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/) is starting to be [merged into Mutter main](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2342) 

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Plasma 6 starts to take shape](https://pointieststick.com/2023/02/03/this-week-in-kde-plasma-6-starts-to-take-shape/)
- Volker Krause: [December/January in KDE Itinerary](https://www.volkerkrause.eu/2023/02/03/kde-itinerary-december-january-2023.html)
- KDE Announcements: [KDE Gear 22.12.2](https://kde.org/announcements/gear/22.12.2/)
- ~redstrate: [My work in KDE for January 2023](https://redstrate.com/blog/2023/02/my-work-in-kde-for-january-2023/)

#### Sxmo
- [Sxmo 1.13.0 released — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3C878rhjwca9.fsf%40momi.ca%3E)

#### Ubuntu Touch
- Nigel, Eline and Shet: [The Triple F 'February-Focal-Fosdem' Newsletter edition!](https://linmob.uber.space/ubports-newsletter-2023-02-05.html)

#### Sailfish OS
- [Release notes: Struven ketju 4.5.0.16 - Announcements - Sailfish OS Forum](https://forum.sailfishos.org/t/release-notes-struven-ketju-4-5-0-16/14290). _Tons of bugs fixed, make sure to check Leszek's video below!_

#### Distributions
- Pine64-Arch: [Arch Linux ARM - 2023/02/03](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20230203) _Nice to see another release! BTW: In a way, Dang attended FOSDEM2023 - the PineTab2 prototype on PINE64's stand ran DanctNIX with KDE Plasma Desktop!_

#### Stack
- Phoronix: [Experimenting Underway To Support Mesa Vulkan Drivers Written In Rust](https://www.phoronix.com/news/Rust-Vulkan-Drivers-Initial)
- Phoronix: [Cairo 1.17.8 Released - OpenGL/GLES Drawing Removed, Better macOS & Windows Support](https://www.phoronix.com/news/Cairo-1.17.8-Released)

#### Linux
- Phoronix: [Linux 6.2-rc7 Released - Stable Kernel Coming In Two Weeks](https://www.phoronix.com/news/Linux-6.2-rc7-Released)

#### Non-Linux
- Lup Yuen: [NuttX RTOS for PinePhone: LVGL Terminal for NSH Shell](https://lupyuen.github.io/articles/terminal)
- Norman Feske for Genodians: [First system image of mobile Sculpt OS](http://genodians.org/nfeske/2023-02-01-mobile-sculpt)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-02-03](https://matrix.org/blog/2023/02/03/this-week-in-matrix-2023-02-03)
- Matrix.org: [Synapse 1.76 released](https://matrix.org/blog/2023/01/31/synapse-1-76-released)

### Worth noting
- [MNT Pocket Reform | Crowd Supply](https://www.crowdsupply.com/mnt/pocket-reform) _The crowdfunder will launch some time this months. Since the Linux on Mobile Stand was quite crowded (at all times I was there), I only have look and did not play with the prototype device, but it looked quite solid!_
- [Robert Mader: "libcamera 0.0.4 "The FOSDEM Release" was just tagged with Pinephone Pro support. This includes support for both cameras and rotations \o/"](https://floss.social/@rmader/109787102271263516) _Impressive! Make sure to check the relevant videos below!_

### Worth reading
- Norman Feske for Genodians: [First system image of mobile Sculpt OS](http://genodians.org/nfeske/2023-02-01-mobile-sculpt). _This is really neat. I've tried it - make sure to read the blog post first carefully._
- Adrian Kingsley-Hughes for ZDNET: [PinePhone Pro Explorer Edition: Great hardware, but the software is for patient Linux pros only](https://www.zdnet.com/article/pinephone-pro-explorer-edition-great-hardware-but-the-software-is-for-patient-linux-pros-only/) _Well, reading is a lost art._
  - [r/MobileLinux thread](https://old.reddit.com/r/mobilelinux/comments/10u86i6/pinephone_pro_explorer_edition_great_hardware_but/)
- Hamblinggreen: [Framebufferphone on Arch Linux ARM](https://hamblingreen.com/2023/01/31/framebufferphone-on-arch-arm.html) _Not for me, but still quite cool!_
- Camden Bruce: [SailfishOS is my new prefered operating system for daily driving the PinePhone](https://medium.com/@camden.o.b/sailfishos-is-my-new-prefered-operating-system-for-daily-driving-the-pinephone-761ea99c1bb5)
- Martijn Braam: [Alpine Linux is pretty neat](https://blog.brixit.nl/alpine-linux-is-pretty-neat/)
- LINux on MOBile: [Looking forward to #FOSDEM2023](https://linmob.net/looking-forward-to-fosdem/) _I am hoping to find the time to write a follow-up how things were._
- OSOR: [Interview: Jörg Wurzer of Volla Phone | Joinup](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/interview-jorg-wurzer-volla-phone)s
- Purism: [FOSDEM 2023 coming up in Brussels](https://puri.sm/posts/fosdem-2023-coming-up-in-brussels/)
- Purism: [Design 2022 in Retrospect](https://puri.sm/posts/design-2022-in-retrospect/)
- Purism: [The Danger of Focusing on Specs](https://puri.sm/posts/the-danger-of-focusing-on-specs/)
- Purism: [Purism Places Purpose Over Profit](https://puri.sm/posts/purism-places-purpose-over-profit/)

### Worth watching

- Nicco Loves Linux: [Daily Driving a KDE Linux Smartphone](https://tube.kockatoo.org/w/806688d7-25f1-4a4f-b3c4-7f5398c9acbc), [YouTube](https://www.youtube.com/watch?v=jfouS0wQ1YM). _Good overview of the current state of Manjaro Plasma Mobile on the PinePhone Pro!_
- Lup Yuen Lee: [Learning to shoot #PinePhone in 4K 🤔](https://www.youtube.com/watch?v=9TgzCd5TELU)
- Ausfaller: [Playing Oregon Trail from a floppy on a Pinephone](https://www.youtube.com/watch?v=x-LvYEKjX78)
- libcamera: [gnome-camera running on Pine Phone Pro with Pipewire and libcamera on Postmarket OS](https://www.youtube.com/watch?v=jnX7awgqPJk)
- Anino ni Kugi: [Convergence - Made You Look (feat. Ubuntu Touch)](https://www.youtube.com/watch?v=zvyU0TN0OEE)
- UBports: [UBports Q&A 122](https://www.youtube.com/watch?v=dfHpACbbjYA) 
- Oren Klopfer: [Ubuntu Touch: PinePhonePro demonstration](https://www.youtube.com/watch?v=xCjAXx9EsSg)
- Leszek Lesner: [SailfishOS 4.5 EA - What's new!?](https://www.youtube.com/watch?v=3jtnytriwfE) _Great release summary!_
- Novaspirit Tech: [KDE Plasma Mobile on Juno Tablet Review](https://www.youtube.com/watch?v=cF34_a8T6_Q)

#### FOSDEM 2023
- Luca Weiss: [Mainline Linux on recent Qualcomm SoCs: Fairphone 4](https://fosdem.org/2023/schedule/event/mainline_on_the_fairphone4/)
- Arnaud Ferraris: [Mobian: to stable... and beyond!](https://fosdem.org/2023/schedule/event/mobian_to_stable_and_beyond/)

_The other recordings of the [FOSS on Mobile Devices devroom](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/) have not been published yet, check the [folder](https://video.fosdem.org/2023/UB4.136/), maybe!_

- Kieran Bingham: [Convergent camera applications for mobile Linux devices](https://fosdem.org/2023/schedule/event/linux_camera_apps/). _This is so cool!_

### Thanks

Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

